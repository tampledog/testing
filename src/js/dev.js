function selectsInit() {
    $('.js-city').styler({
        selectSmartPositioning: false
    });
    $('.js-callback').styler({
        selectSmartPositioning: false,
        onSelectClosed: function() {
            var link = $('.js-call-link');
            var value = $(this).find('.jq-selectbox__select-text').text();
            link.text(value).attr('href', 'tel:' + value);
        }
    });
    $('.js-pick-select').styler({
        selectSmartPositioning: false,
    })
}

$('.js-filter').on('click', function(){
    if (filtered === false) {
        $('.js-pick-slider').slick('slickFilter',':even');
        $(this).text('Unfilter Slides');
        filtered = true;
    } else {
        $('.js-pick-slider').slick('slickUnfilter');
        $(this).text('Filter Slides');
        filtered = false;
    }
});


function changePrewiev() {
    $(document).on('click', '.item__mini', function (e) {
        e.preventDefault();
        var item = $(this);
        var link = item.attr('href');
        var frame = $('video');
        if (item.hasClass('active')) {
            return false;
        } else {
            $('.item__mini').removeClass('active');
            item.addClass('active');
            if (item.hasClass('vid')) {
                frame.addClass('active');
                $('.item__large img').removeClass('active');
                $('video source').attr('src', link);

            } else {
                $('.item__large img').attr('src', link).addClass('active');
                var vid = document.getElementById("vid");
                vid.pause();
                frame.removeClass('active');
            }
        }
    })
}


function likeSliderInit() {
    var item = $('.js-kind-slider');
    if (item.length) {
        item.slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button class="arrow"></button>',
            nextArrow: '<button class="arrow arrow--next"></button>',
            appendArrows: $('.js-kind-arr'),
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 840,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}


function changeTabs() {
    $(document).on('click', '.tabs__head', function (e) {
        e.preventDefault();
        var tab = $(this);
        var box = tab.parents('.tabs');
        var index = $(this).index();
        console.log(index);
        if (tab.hasClass('active')) {
            return false;
        } else {
            box.find('.tabs__head').removeClass('active');
            tab.addClass('active');
            box.find('.tabs__body').removeClass('active');
            box.find('.tabs__body').eq(index).addClass('active');
        }
        massSliders();
    });
}

function showTable() {
    $(document).on('click', '.js-show-table', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.characters__table').animate({
                'max-height': '308px'
            })
        } else {
            $(this).addClass('active');
            $('.characters__table').animate({
                'max-height': '1000px'
            })
        }
    })
}

function showDesc() {
    $(document).on('click', '.js-show-desc', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.characters__desc').animate({
                'max-height': '130px'
            })
        } else {
            $(this).addClass('active');
            $('.characters__desc').animate({
                'max-height': '1000px'
            })
        }
    })
}


function likeDis() {
    $(document).on('click', '.js-like', function () {
        var like = $(this);
        if (like.hasClass('active')) {
            like.removeClass('active');
            like.next('span').text( parseInt(like.next('span').text()) - 1);
        } else {
            like.addClass('active');
            like.next('span').text( parseInt(like.next('span').text()) + 1);
        }
    });
    $(document).on('click', '.js-dis', function () {
        var dis = $(this);
        if (dis.hasClass('active')) {
            dis.removeClass('active');
            dis.next('span').text( parseInt(dis.next('span').text()) - 1);
        } else {
            dis.addClass('active');
            dis.next('span').text( parseInt(dis.next('span').text()) + 1);
        }
    })
}

function checkTheText() {
    $('.question__text').each(function () {
        if ($(this).find('.question__chapter p').outerHeight() > 80) {
            console.log($(this).find('.question__chapter p').outerHeight());
            $(this).find('.button').removeClass('hidden');
        } else {
            $(this).find('.button').addClass('hidden');
        }
    })
}

function commentsShower() {
    $(document).on('click', '.js-show-comment', function () {
        var button = $(this);
        if (button.hasClass('active')) {
            button.removeClass('active');
            button.parent().find('.question__chapter').animate({
                'max-height' : '72px'
            })
        } else {
            button.addClass('active');
            button.parent().find('.question__chapter').animate({
                'max-height' : '600px'
            })
        }
    })
}

function showAllComments() {
    $(document).on('click', '.js-show-total', function () {
        var button = $(this);
        if (button.hasClass('active')) {
            button.removeClass('active');
            button.parents('.tabs__body').find('.toggle').removeClass('display');
        } else {
            button.addClass('active');
            button.parents('.tabs__body').find('.toggle').addClass('display');
        }
    })
}

function initArticlesSlider() {
    var slider = $('.js-art-slider');
    if (slider.length) {
        slider.slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            variableWidth: true,
            prevArrow: '<button class="arrow"></button>',
            nextArrow: '<button class="arrow arrow--next"></button>',
            appendArrows: $('.articles__arrows'),
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 840,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    }
}

function massSliders() {
    var slider = $('.filters .tabs__body.active .js-mass-slider');
    var box = slider.parents('.tabs__body').find('.engine');
    if (slider.length) {
        slider.slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button class="arrow"></button>',
            nextArrow: '<button class="arrow arrow--next"></button>',
            appendArrows: box,
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 840,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

function pickSliderInit() {
    var item = $('.js-pick-slider');
    if (item.length) {
        item.slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button class="arrow"></button>',
            nextArrow: '<button class="arrow arrow--next"></button>',
            appendArrows: $('.pick-arrows'),
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 840,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

function watchedSliderInit() {
    var item = $('.js-watched-slider');
    if (item.length) {
        item.slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            variableWidth: true,
            prevArrow: '<button class="arrow"></button>',
            nextArrow: '<button class="arrow arrow--next"></button>',
            appendArrows: $('.watched-arrows'),
            responsive: [
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1050,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 840,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}


$(document).ready(function () {
    watchedSliderInit();
    pickSliderInit();
    massSliders();
    showAllComments();
    commentsShower();
    checkTheText();
    likeDis();
    showDesc();
    showTable();
    changeTabs();
    likeSliderInit();
    selectsInit();
    changePrewiev();
    initArticlesSlider();
    $(document).on("click","a.anchor", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 40}, 1500);
    });
});